const menuBarDiv = document.querySelector(".open-menu-div");
const navBar = document.querySelector(".navbar-item")
const navBarUl = document.querySelector("nav ul")
const navBarListItem = document.querySelectorAll("nav ul li")
const logo = document.querySelector(".logo")

menuBarDiv.onclick = function () {
    menuBarDiv.classList.toggle("active");
    navBar.classList.toggle("navbar-item")
    navBar.classList.toggle("show-nav-menu-on-mobile")
    logo.classList.toggle("invisible")

//    REMOVE THE STYLE OFF THE NAV UL
    navBarUl.classList.toggle("display-block")

    for (let i = 0; navBarListItem.length; i++) {
        navBarListItem[i].classList.toggle("display-block")
        navBarListItem[i].classList.toggle("text-center")
    }

};